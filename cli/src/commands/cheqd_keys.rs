extern crate regex;
extern crate chrono;

use crate::command_executor::{Command, CommandContext, CommandMetadata, CommandParams, CommandGroup, CommandGroupMetadata};
use crate::commands::*;

use crate::libvdrtools::cheqd_keys::CheqdKeys;
use serde_json;

pub mod group {
    use super::*;

    command_group!(CommandGroupMetadata::new("cheqd-keys", "Cheqd keys management commands"));
}

pub mod add_command {
    use super::*;

    command!(CommandMetadata::build("add", "Add key to wallet.")
                .add_required_param("alias", "Alias of key.")
                .add_optional_deferred_param("mnemonic", "Mnemonic phrase for creation key.")
                .add_optional_param("passphrase", "It's needed for recover key which was created with passphrase. By default it's empty string")
                .add_example("cheqd-keys add alias=my_key")
                .add_example("cheqd-keys add alias=my_key mnemonic")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);

        let wallet_handle = ensure_opened_wallet_handle(&ctx)?;
        let alias = get_str_param("alias", params).map_err(error_err!())?;
        let mnemonic = get_opt_str_param("mnemonic", params).map_err(error_err!())?;
        let passphrase = get_opt_str_param_can_be_empty("passphrase", params)?.unwrap_or("");

        let res = if let Some(mnemonic)  = mnemonic {
            match CheqdKeys::add_from_mnemonic(wallet_handle, alias, mnemonic, passphrase) {
                Ok(resp) => {
                    let resp = serde_json::from_str::<serde_json::Value>(resp.as_str())
                        .map_err(|_| println_err!("Unexpected internal error while parsing internal response"))?;
                    let account_id = resp["account_id"].as_str().clone().unwrap();
                    println_succ!("Key for account: {}", account_id);
                    println_succ!("has been restored from mnemonic");
                    Ok(())
                },
                Err(err) => {
                    handle_indy_error(err, None, None, None);
                    Err(())
                }
            }
        }
        else {
            match CheqdKeys::add_random(wallet_handle, alias) {
                Ok(resp) => {
                    let resp = serde_json::from_str::<serde_json::Value>(resp.as_str())
                        .map_err(|_| println_err!("Unexpected error while parsing internal response"))?;
                    let account_id = resp["account_id"].as_str().clone().unwrap();
                    let mnemonic = resp["mnemonic"].as_str().clone().unwrap();
                    println_succ!("Random key has been added.");
                    println_succ!("Account_id is: \"{}\"", account_id);
                    println_succ!("**Important** write this mnemonic phrase in a safe place.");
                    println_succ!("Mnemonic phrase is:");
                    println_succ!("{}", mnemonic);
                    Ok(())
                },
                Err(err) => {
                    handle_indy_error(err, None, None, None);
                    Err(())
                },
            }
        };

        trace!("execute << {:?}", res);
        res
    }
}

pub mod get_info_command {
    use super::*;

    command!(CommandMetadata::build("get-info", "Get info about key.")
                .add_required_param("alias", "Alias of key.")
                .add_example("cheqd-keys get-info alias=my_key")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);

        let wallet_handle = ensure_opened_wallet_handle(&ctx)?;
        let alias = get_str_param("alias", params)
            .map_err(error_err!())?;

        let res = match CheqdKeys::get_info(wallet_handle, alias) {
            Ok(resp) => {
                let resp = serde_json::from_str::<serde_json::Value>(resp.as_str())
                    .map_err(|_| println_err!("Unexpected error while parsing internal response"))?;
                let account_id = resp["account_id"].as_str().clone().unwrap();
                let pub_key = resp["pub_key"].as_str().clone().unwrap();
                println_succ!("Account ID is: {}", account_id);
                println_succ!("Public key is: {}", pub_key);
                Ok(())
            },
            Err(err) => {
                handle_indy_error(err, None, None, None);
                Err(())
            },
        };

        trace!("execute << {:?}", res);
        res
    }
}

pub mod list_command {
    use super::*;
    use crate::utils::table::print_list_table;

    command!(CommandMetadata::build("list", "Get list keys in the current wallet.")
                .add_example("cheqd-keys list")
                .finalize()
    );

    fn execute(ctx: &CommandContext, params: &CommandParams) -> Result<(), ()> {
        trace!("execute >> ctx {:?} params {:?}", ctx, params);

        let wallet_handle = ensure_opened_wallet_handle(&ctx)?;

        let res = match CheqdKeys::list(wallet_handle) {
            Ok(resp) => {
                let resp: Vec<serde_json::Value> = serde_json::from_str(&resp)
                    .map_err(|_| println_err!("{}", format!("Wrong data has been received: {}", resp)))?;

                print_list_table(&resp,
                                 &[("account_id", "Account id"),
                                     ("alias", "Alias"),
                                     ("pub_key", "Public key")],
                                 "There are no configs");
                Ok(())
            },
            Err(err) => {
                handle_indy_error(err, None, None, None);
                Err(())
            },
        };

        trace!("execute << {:?}", res);
        res
    }
}

#[cfg(test)]
pub mod tests {
    use super::*;

    const KEY_ALIAS: &str = "key_alias";
    const MNEMONIC: &str = "mnemonic";
    const PASSPHRASE: &str = "";
    pub(crate) const KEY_ALIAS_WITH_BALANCE: &str = "alice";
    pub(crate) const MNEMONIC_WITH_BALANCE: &str = "sketch mountain erode window enact net enrich smoke claim kangaroo another visual write meat latin bacon pulp similar forum guilt father state erase bright";


    mod cheqd_keys {
        use super::*;

        #[test]
        pub fn add_random() {
            let ctx = setup_with_wallet();
            {
                let cmd = add_command::new();
                let mut params = CommandParams::new();
                params.insert("alias", KEY_ALIAS.to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        pub fn add_from_mnemonic() {
            let ctx = setup_with_wallet();
            {
                let cmd = add_command::new();
                let mut params = CommandParams::new();
                params.insert("alias", KEY_ALIAS_WITH_BALANCE.to_string());
                params.insert("mnemonic", MNEMONIC_WITH_BALANCE.to_string());
                params.insert("passphrase", PASSPHRASE.to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        pub fn get_info() {
            let ctx = setup_with_wallet_and_cheqd_pool();
            {
                let cmd = get_info_command::new();
                let mut params = CommandParams::new();
                params.insert("alias", KEY_ALIAS_WITH_BALANCE.to_string());
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }

        #[test]
        pub fn list() {
            let ctx = setup_with_wallet_and_cheqd_pool();
            {
                let cmd = list_command::new();
                let params = CommandParams::new();
                cmd.execute(&ctx, &params).unwrap();
            }
            tear_down_with_wallet(&ctx);
        }
    }

    pub fn add(ctx: &CommandContext) {
        {
            let wallet_handle = ensure_opened_wallet_handle(&ctx).unwrap();
            CheqdKeys::add_from_mnemonic(wallet_handle,
                                         KEY_ALIAS_WITH_BALANCE,
                                         MNEMONIC_WITH_BALANCE,
                                         PASSPHRASE).unwrap();
        }
    }

    pub fn get_key(ctx: &CommandContext) -> serde_json::Value {
        let wallet_handle = ensure_opened_wallet_handle(ctx).unwrap();
        let key = CheqdKeys::get_info(wallet_handle, KEY_ALIAS_WITH_BALANCE).unwrap();
        serde_json::from_str(&key).unwrap()
    }
}