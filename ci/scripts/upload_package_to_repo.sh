#!/bin/bash
set +x
set -e
if [ $# -ne 4 ]; then
    echo "USAGE: $0 CREDENTIALS FILE URL VERSION"
    exit 1
fi

TOKEN=$1
FILENAME=$2
URL=$3
VERSION=$4

echo "Filename: ${FILENAME}"
echo "TYPE: ${TYPE}"
echo "URL: $URL"

echo 'info:'
pwd
ls -al
find "./output" -type f -name ${FILENAME}
echo 'end info'

artifact=`find "./output" -type f -name ${FILENAME}`
curl --header "JOB-TOKEN: ${TOKEN}" --upload-file $artifact ${URL}/${VERSION}/$(basename $artifact) || exit 1;


