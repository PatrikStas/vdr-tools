import asyncio
import json

import pytest

from vdrtools import did
from vdrtools.vdr import VdrBuilder


@pytest.mark.asyncio
async def test_vdr_open_indy_and_cheqd_pools(wallet_handle,
                                             indy_namespace,
                                             indy_namespaces,
                                             pool_genesis_txn_data,
                                             cheqd_namespace,
                                             cheqd_namespaces,
                                             cheqd_chain_id,
                                             cheqd_pool_ip,
                                             seed_trustee1,
                                             did_trustee):
    vdr_build = VdrBuilder.create()
    await vdr_build.register_indy_ledger(indy_namespaces, pool_genesis_txn_data)
    await vdr_build.register_cheqd_ledger(cheqd_namespaces, cheqd_chain_id, cheqd_pool_ip)
    vdr = vdr_build.finalize()

    (trustee_did, _) = \
        await did.create_and_store_my_did(wallet_handle,
                                          json.dumps({"seed": seed_trustee1, "method_name": indy_namespace}))

    namespaces = json.dumps([indy_namespace, cheqd_namespace])
    await vdr.ping(namespaces)

    did_docs = await asyncio.gather(*[vdr.resolve_did(trustee_did) for i in range(0, 5)])
    assert len(did_docs) == 5

    did_doc = json.loads(did_docs[0])
    assert did_doc['did'] == did_trustee
