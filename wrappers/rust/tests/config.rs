extern crate vdrtoolsrs as vdrtools;

mod tests {
    use super::*;

    #[test]
    fn set_runtime_config_works () {
        vdrtools::set_runtime_config(r#"{"crypto_thread_pool_size": 2}"#);
    }
}
