use std::pin::Pin;

use futures::Future;

use crate::{ErrorCode, IndyResult};

use crate::ffi::metrics;

use crate::utils::callbacks::{ClosureHandler, ResultHandler};

use crate::ffi::ResponseStringCB;
use crate::CommandHandle;

/// Collect metrics from libindy.
///
/// # Returns
/// String with a dictionary of metrics in JSON format. Where keys are names of metrics.
pub fn collect_metrics() -> Pin<Box<dyn Future<Output=IndyResult<String>> + Send>> {
    let (receiver, command_handle, cb) = ClosureHandler::cb_ec_string();

    let err = _collect_metrics(command_handle, cb);

    ResultHandler::str(command_handle, err, receiver)
}

fn _collect_metrics(command_handle: CommandHandle, cb: Option<ResponseStringCB>) -> ErrorCode {
    ErrorCode::from(unsafe {
      metrics::indy_collect_metrics(command_handle, cb)
    })
}
