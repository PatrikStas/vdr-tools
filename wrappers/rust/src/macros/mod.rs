macro_rules! c_str {
    ($x:ident) => {
        std::ffi::CString::new($x).unwrap()
    };
    ($x:expr) => {
        std::ffi::CString::new($x).unwrap()
    }
}

macro_rules! opt_c_str {
    ($x:ident) => {
        $x.map(|s| std::ffi::CString::new(s).unwrap())
            .unwrap_or(std::ffi::CString::new("").unwrap())
    }
}

macro_rules! opt_c_str_json {
    ($x:ident) => {
        $x.map(|s| std::ffi::CString::new(s).unwrap())
            .unwrap_or(std::ffi::CString::new("{}").unwrap())
    }
}

macro_rules! opt_c_ptr {
    ($x:ident, $y:ident) => {
        if $x.is_some() { $y.as_ptr() } else { std::ptr::null() }
    }
}

macro_rules! opt_u64 {
    ($x:ident) => {
        $x.map(|x| x as i64).unwrap_or(-1)
    }
}

macro_rules! rust_str {
    ($x:ident) => {
        unsafe { std::ffi::CStr::from_ptr($x).to_str().unwrap().to_string() }
    }
}

macro_rules! opt_rust_str {
    ($x:ident) => {
        if $x.is_null() {
            None
        } else {
            Some(unsafe { std::ffi::CStr::from_ptr($x).to_str().unwrap().to_string() })
        }
    };
}

macro_rules! rust_slice {
    ($x:ident, $y:ident) => {
        unsafe { ::std::slice::from_raw_parts($x, $y as usize) }
    }
}
