﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Threading.Tasks;

namespace Com.Evernym.Vdrtools.Test.PoolTests
{
    [TestClass]
    public class RefreshPoolTest : IndyIntegrationTestBase
    {
        [TestMethod]
        public async Task TestRefreshPoolWorks()
        {
            var pool = await PoolUtils.CreateAndOpenPoolLedgerAsync();

            Assert.IsNotNull(pool);
            openedPools.Add(pool);

            await pool.RefreshAsync();
        }       
    }
}
