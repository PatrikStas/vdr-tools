{
  "targets": [
    {
      "target_name": "vdrtools",
      "include_dirs": [
        "<!(node -e \"require('nan')\")",
        "<(module_root_dir)/include",
        "<(module_root_dir)/../../libvdrtools/include"
      ],
      "sources": [
        "src/vdr.cc"
      ],
      "link_settings": {
        "conditions": [
          [ 'OS=="win"',
            {
              'library_dirs': [
                "<(module_root_dir)",
                "<!(node -e \"console.log(process.env.LD_LIBRARY_PATH || '')\")"
              ],
              'libraries': [
                "vdrtools.dll.lib"
              ]
            },
            {
              'libraries': [
                "-L<(module_root_dir)",
                "<!(node -e \"console.log((process.env.LD_LIBRARY_PATH || '').split(':').map(a => '-L' + a.trim()).filter(a => a != '-L').join(' '))\")",
                "-lvdrtools"
              ],
            },
          ]
        ],
      }
    }
  ]
}
