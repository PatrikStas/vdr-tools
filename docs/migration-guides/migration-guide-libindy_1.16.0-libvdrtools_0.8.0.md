# How to migrate
### Libvdrtools
Libvdrtools version 0.8.0 (the initial release) is a drop in, backwards compatible replacement
for libindy. Libvdrtools produces artifacts with the same name as libindy, and thus conflicts with it. 
As such, it will be necessary to remove libindy from your system before installing libvdrtools. If you
have Libnullpay and/or Indy-CLI installed you will also need to remove them as they depend on libindy. 
After conflicting packages have been removed you can follow the installation instructions for libvdrtools foud
[here](../../README.md#Installing)

#### Uninstall Indy-Sdk packages

    sudo apt remove {package}

Where 'package' is libindy, libnullpay, or indy-cli.